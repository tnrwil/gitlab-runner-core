variable "aws_region"{
  default = "us-east-1"
}

variable "aws_ami"{
  #default="ami-5a740e3b"
  #default="ami-1c81ce7d"
  #default="ami-e7063586"
  default="ami-098f16afa9edf40be"
}

variable "instance_count" {
  default="1"
}
variable "ec2_type" {
  default="t2.small"
}
variable "ec2_key_name" {
  default="mytest-ebs"
}

variable "pub_key" {
  default="mytest-ebs.pem"
}

variable "pub_key_bucket_name" {
  default="cie-mis"
}

variable "ssh_user" {
  default="ec2-user"
}

variable "ebs_vol_size" {
  default="10"
}

#variable "private_subnet" {
#  #TestVPC
# # default="subnet-9d3d7deb"
#  #PRODVPC
##  default="subnet-ce688187"
#  #CoreVPC
##  default="subnet-3819e971"
#  #DevVPC
##   default="subnet-802868f6"
#  #Tam's private subnet
#   default="subnet-3492bd6e"
#
#}

variable "public_subnet" {
  #TestVPC
#  default="subnet-3b37774d"
  #PRODVPC
  #default="subnet-c606ef8f"
  #CoreVPC
#  default="subnet-8e2edec7"
  #DevVPC
#  default="subnet-ee3a7a98"
#  Tam's private subnet
   default="subnet-3492bd6e"
}

variable "vpc" {
  #default = "vpc-262b2042"
  #ProdVPC
  #default = "vpc-4b9e9a2f"
  #DevVPC
  #default = "vpc-1fce497b"
  #TestVPC
  #default  = "vpc-8ad750ee"
  #Tameika's VPC
  default = "vpc-20e5f259"
}


variable "security_groups" {
  type    = list(string)
#  default = ["sg-12068674","sg-17e1216e","sg-d102c2a8"]
#  ProdVPC
#  default = ["sg-6423e31d"]
#  default = ["sg-c9ac00b0"]
#  DevVPC
#   default = ["sg-ba49c7dc"]
#  TestVPC
#   default= ["sg-d1b2a5b6"]
#  Tam's
   default = ["sg-6ff7521c"]
}
